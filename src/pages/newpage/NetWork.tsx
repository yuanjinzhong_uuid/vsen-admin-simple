import React, {useEffect, useState} from "react";
import {PageContainer} from "@ant-design/pro-layout";
import {PageHeader} from "antd";
import {request} from "@@/plugin-request/request";


const NetWork: React.FC = () => {
  const [content, setContent] = useState("");

  useEffect(() => {
    request("/api/oauth/admin").then(function (response) {
      console.log(response);
      setContent(response);
    });
  }, [])

  return (
    <PageHeader>
      我是header
      <PageContainer>
        {JSON.stringify(content)}
      </PageContainer>
    </PageHeader>
  )
};

export default NetWork;
