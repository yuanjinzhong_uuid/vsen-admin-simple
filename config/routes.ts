﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    component: './Welcome',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    component: './Admin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    name: '新增页面',
    icon: 'smile',
    /**
     * Any valid URL path
     */
    path: '/newpage',
    /**
     * A React component to render only when the location matches.
     */
    component: './newpage'

  },
  {
    name: '该页面发起网络请求',
    icon: 'smile',
    /**
     * Any valid URL path
     */
    path: '/network',
    /**
     * A React component to render only when the location matches.
     */
    component: './newpage/network'
  },
  {
    component: './404',
  },

];
